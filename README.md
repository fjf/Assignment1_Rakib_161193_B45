 Making a game with SCRATCH 
--------------------------

Scratch একটি প্রোগ্রামিং ল্যাঙ্গুয়েজ, যার সাহায্যে আমরা নিজস্ব ইন্টারএকটিভ স্টোরি, এনিমেশন, গেম, মিউজিক, আর্ট ইত্যাদি তৈরি করতে পারি। এখন আমি scratch দিয়ে একটা সিম্পল গেম তৈরি করব। এজন্য যা করব- 

১। https://scratch.mit.edu ওয়েব ঠিকানায় গিয়ে সাইন ইন করব। 

২। নতুন প্রজেক্ট শুরু করার জন্য create বাটনে ক্লিক করব। 

৩। একটা ইন্টারফেস আসবে, এর ওপরের খালি ঘরে প্রজেক্টের নাম লিখব। 

Interface
---------

১। Scratch interface দুটো সেকশনে বিভক্ত। ওপরে বামে প্রজেক্টের ব্যাকগ্রাউন্ড অংশকে stage বলে, যেখানে scratch প্রজেক্ট physically run করে। স্টেজের মধ্যে By default একটি scratch Cat থাকে, যা sprite নামে পরিচিত। এটা হল একটা অবজেক্ট, যা স্ক্রিপ্টের মাধ্যমে কিছু ফাংশন পারফর্ম করে। আমরা অন্য sprite ব্যবহার করব, তাই এই বেড়ালটাকে রিমুভ করতে হবে। সেজন্য ওপরের টুলবারে সিজার আইকনে ক্লিক করে, তারপর বেড়ালের ওপর ক্লিক করলে এটা রিমুভ হয়ে যাবে। 
  
২। Stage এর মধ্যে script, sound এবং backdrop থাকে। 
Script হল কতগুলো block এর সেট। Block এর মধ্যে কিছু specific command থাকে, যা পৃথকভাবে কাজ করে। 

৩। ডানে থাকে script area, যেখানে script সাজানো হয়।  

৪। Backdrop হল stage এর ফ্রেম বা ব্যাকগ্রাউন্ড। এখন নতুন backdrop নেয়ার জন্য পেজের নিচে বাম কোনার বাটনে ক্লিক করব। তখন একটা pre-built backdrops library আসবে। সেখান থেকে blue sky সিলেক্ট করব। 

৫। এখন নতুন sprite নেয়ার জন্য stage এর নিচে বাম পাশের বাটনে ক্লিক করব। তখন একটা pre-built sprite library আসবে। সেখান থেকে cheesy-pufs এবং bowl সিলেক্ট করব। 

Programming- backdrop
---------------------

১। Script এর control অংশ থেকে প্রথমে forever ব্লক টেনে ডানে Scripts area তে ছেড়ে দেব।  

২। Sound সংযোজনের জন্য sound অংশ থেকে play sound until done ব্লক নেব। ব্লকের মাঝখানের drop down মেনু থেকে record সিলেক্ট করব। এরপর sound library থেকে dance magic সিলেক্ট করব। এই ব্লকটি forever ব্লকের মধ্যে ঢোকাব। Block এ ক্লিক করার পর আমরা দেখতে পাবো এটি নুতন Sound প্লে করছে। 

৩। এখন SCRIPTS এর EVENTS এর উপর CLICK করব। একটি when clicked block নিয়ে stack এর উপর যোগ করব। আমাদের SCRIPT টি start হবে, যখন আমরা green flag এ click করবো।

৪। এখন আরেকটি when clicked block নেব। তার নিচে wait 1 secs ব্লক বসাব। সেটা ৩ সেকেন্ডে পরিবর্তিত করব। 

৫। এর নিচে একটা switch backdrop to ব্লক বসাব। এরপর আগের মতো backdrop library তে গিয়ে space backdrop সিলেক্ট করব। Switch backdrop to ব্লকের drop down মেনু থেকে space সিলেক্ট করব। এখন গ্রীন ফ্ল্যাগ এর উপর ক্লিক করলে দেখতে পাবো যে, মিউজিক শুরু হয়ে ৩ সেকেন্ড পর backdrop পরিবর্তিত হয়ে গেছে। 

Programming- sprite 1
---------------------

১। প্রথমে when backdrop switches to ব্লক নেব, এবং এর drop down মেনু থেকে space সিলেক্ট করব। তার নিচে set score to 0 এবং show ব্লক বসাব। এর নিচে go to ব্লক নিয়ে তার মান random position দেব। এর নিচে set y to ব্লক নিয়ে মান 180 বসাব। 

২। তার নিচে একটা forevrer ব্লক নিয়ে তার মধ্যে change y by ব্লক নিয়ে তার মান -5 দেব। তার নিচে if…then ব্লক নেব। তার মধ্যে প্রয়োজনীয় ব্লক নিয়ে একটা statement তৈরি করব এভাবে- if y position <170, then… go to random position and set y to 180. 

৩। এই ব্লক সমূহ বসানোর ফলে, যখন backdrop পরিবর্তিত হবে এবং স্কোর জিরো থাকবে, তখন cheesy-pufs sprite শো করবে। এটা random position থেকে চলা শুরু করবে এবং y অক্ষ ধরে 180 মান পর্যন্ত এর যাত্রাপথ set করা থাকবে। যখন নিচে আসতে আসতে এর মান -170 এর কম হবে, তখন এটা আবার আগের মতো প্রথম থেকে যাত্রা শুরু করবে, এবং এটা এভাবে forever চলবে। 

৪। এখন when clicked ব্লক নিয়ে, তার নিচে set score to 0 এবং hide বসাব। 

৫। এরপর when backdrop switches to space ব্লক তৈরি করে তার নিচে forever ব্লক, এবং তার মধ্যে if…then ব্লক বসাব। তার মধ্যে প্রয়োজনীয় ব্লক নিয়ে একটা statement তৈরি করব এভাবে- if touching bowl, then… change score by 1, go to random position and set y to 180. 

৬। অর্থাৎ, cheesy-pufs sprite যদি bowl sprite কে টাচ করে, তাহলে স্কোর এক বাড়বে, এরপর cheesy-pufs স্প্রাইট random position এ যাবে এবং y অক্ষ ধরে 180 মান পর্যন্ত সেট হবে। 

Programming- sprite 2
----------------------

১। পরবর্তী spite এর ক্ষেত্রেও when clicked ব্লক নিয়ে তার নিচে hide ব্লক বসাব।

২। এরপর when backdrop switches to space ব্লক তৈরি করে তার নিচে show, এবং তার নিচে forever ব্লক বসাব। এরপর forever এর মধ্যে প্রয়োজনীয় ব্লক নিয়ে দুটো statement তৈরি করব এভাবে- if key left arrow pressed, then… x change by -10
              এবং if key right arrow pressed, then… x change by 10. 

৩। অর্থাৎ, প্রোগ্রামে ক্লিক করার পরও bowl হাইড থাকবে, এরপর যখন backdrop পরিবর্তিত হবে তখন তা show করবে। তখন লেফট কি চাপলে bowl বাম দিকে ১০ ঘর যাবে, আর রাইট কি চাপলে ডান দিকে ১০ ঘর যাবে, এবং এটা forever চলতে থাকবে। রেড বাটনে ক্লিক করলে প্রোগ্রাম স্টপ হবে। 

---------------------------     -------------------------------    ---------------------------------    ----------------------------------------------
Project submitted by
---------------------
       Mustafa Rakib Hasan
       SEID No. 161193
       Batch- 45
       Assignment no. 1
